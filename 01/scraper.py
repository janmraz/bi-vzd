# import all the neccessary libs
import requests
import time
from bs4 import BeautifulSoup
import pandas as pd
from sys import argv
import sys

if len(sys.argv) < 4:
    sys.exit("Wrong number of input parameters - " + str(len(sys.argv))  +", it should be python3 scraper.csv <min_id> <max_id> <export_file>")

arg1, arg2, arg3, arg4 = argv

tmp = []

# vote_id
min_vote_id = int(arg2)
max_vote_id = int(arg3)

# iterate over all the votes within the range
for vote_id in range(min_vote_id, max_vote_id + 1):
    try:
        # await for a while to prevent blocking from the provider
        time.sleep(0.5)
        start = time.time()

        # make http get request
        url = 'https://www.psp.cz/sqw/hlasy.sqw?g=' + str(vote_id)
        r = requests.get(url)

        # parse the document into the soup :)
        soup = BeautifulSoup(r.text, 'html.parser')

        # hack to extract title of voting procedure
        vote_title = ''.join(soup.find('h1').find('br').next_siblings)
        vote_description = soup.find('h1').next_element.strip().split(',')[0]

        # focus on the main part of the page (to prevent finding incorrect elements)
        soup_main = soup.find('div', {"id": "main-content"})

        # iterate over all the parties (+ remove first and last title - clear the data)
        for party in soup_main.find_all('h2', {"class": "section-title"})[1:-1]:
            # extract the name of the party
            party_name = party.span.find(text=True, recursive=False).split(sep=' ')[0]

            # iterate over all the member of parlaments of the current party
            for mp in party.findNext('ul'):
                # extract name and vote

                mp_name = mp.get_text().split(' ')[1].split('(')[0]
                mp_vote = mp.get_text().split(' ')[0]

                # create row object and then insert into dataframe
                row = {'Vote description': vote_description, 'Vote Id': vote_id, 'Vote Title': vote_title,
                       'Party': party_name, 'MP': mp_name, 'Vote': mp_vote}
                tmp.append(row)

                # print the result and time of execution
        end = time.time()
        print(vote_description + ' ' + vote_title + ' ' + str(vote_id) + ' time: ' + str(end - start))
    except Exception as e:
        print("exception", end=' ')
        print(e)

df = pd.DataFrame(tmp, columns=['Vote description', 'Vote Id', 'Vote Title', 'Party', 'MP', 'Vote'])

# export data
df.to_csv(arg4, index=False)

print("export is done to " + arg4)