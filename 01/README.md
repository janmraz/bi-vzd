# Domácí úkol na BI-VZD
Jedná se o webovou stránku s interaktivními grafy postavené na platformě Plotly (která využívá Flask mimojiné)
## Jak spustit
* nainstalovat minicondu
* nainstalovat závislosti 

```conda env update```

* Aktivovat prostředí

```conda activate bivzd```

* spustit v shellu python aplikaci s argumentem ohledně zdrojového souboru

```python3 app.py export.csv```

* otevřít v prohlížeči adresu http://127.0.0.1:8050/

## Jak spustit crawler
* spustit v shellu python aplikaci s argumentem ohledně zdrojového souboru

```python3 app.py <min_vote_id> <max_vote_id> <export_file>```

* například pak tedy

```python3 scraper.py 67018 74266 tmp.csv ```

## Co je to min a max vote id
Jedná se o url argumenty jednotlivých stránek. Například id této stránky https://www.psp.cz/sqw/hlasy.sqw?g=67018&l=cz
je argument g - tedy id=67018. Pokud bychom pak šli na následující hlasování pak v url bude argument o jedničku větší.
A proto se v crawleru používá max a min vote id, které stáhnou všechny hlasování mezi min a max id.
 
## Použití
Soubor ```app.py``` obsahuje samotný program, ale kvůli tomu
že původně byl vyvíjen v rámci Jupyter notebook tak je tu přitomen i původní ```jupyter notebook```. 
Můžeme tedy porovnat sílu a interaktivitu mezi ```plotly``` a ```seaborn```.
Kvůli velikosti (a délce stahování) zdrojového souboru je v repozitáři i ```export.csv```.
Samotný scraper je v souboru ```scraper.py```.
