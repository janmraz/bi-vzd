# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
import dash
import dash_table
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np
import plotly.graph_objs as go
import random
from sys import argv
import sys

if len(sys.argv) < 2:
    sys.exit("Wrong number of input parameters - " + str(len(sys.argv))  +", it should be python3 scraper.csv <min_id> <max_id> <export_file>")

arg1, arg2 = argv

pd.options.plotting.backend = "plotly"

# create dash app
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

# load the data from saved
df = pd.read_csv(arg2)

# min vote id
min_vote_id = df["Vote Id"].min()

# TODO fetch real-time data
# load data, get maximum id and then try to scrape next ones until you fail, save data afterwards

# update data according to our needs
df.MP = df.MP + " (" + df.Party + ")"
df["Vote Id"] = df["Vote Id"] - min_vote_id + 1

# create partial data frames
df_a = df[df.Vote == "A"]
df_n = df[df.Vote == "N"]
df_z = df[df.Vote == "Z"]
df_0 = df[df.Vote == "0"]
df_m = df[df.Vote == "M"]

# attendance of each party in percent
attendance_data = df.drop(columns=["Vote description", "Vote Id"])
attendance_data['MP'] = 1
attendance_data['Vote'] = attendance_data.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)
attendance = attendance_data.groupby("Party").sum()
attendance['result'] = attendance.Vote / attendance.MP
attendance = attendance.drop(columns=['Vote', "MP"]).sort_values("result", ascending=False)
attendance_fig = attendance.plot(kind='bar')
attendance_fig['layout']['yaxis1'].update(title='', range=[0.7, 0.9])
attendance_fig.update_layout(showlegend=False)
attendance_fig.update_xaxes(title_text='Jednotlivé strany')
attendance_fig.update_yaxes(title_text='Procentuální účast poslanců jednotlivých stran')


# each mps attendance in time
def mps_attendance(chosen_mps):
    if not chosen_mps:
        return {}
    mps = df[df.MP.isin(chosen_mps)].drop(columns=["Party", "Vote description"])
    mps.Vote = mps.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)
    mps = pd.pivot_table(mps, values='Vote', index=['Vote Id'], columns='MP', aggfunc=np.sum)

    mps_fig = go.Figure()
    for col in mps.columns:
        mps_fig.add_trace(go.Scatter(x=mps.index, y=mps[col].rolling(500).mean(),
                                     name=col,
                                     mode='markers+lines',
                                     line=dict(shape='linear'),
                                     connectgaps=True
                                     )
                          )

    mps_fig.update_xaxes(title_text='Jednotlivá hlasování')
    mps_fig.update_yaxes(title_text='Procentuální účast poslanců')
    mps_fig.update_layout(hovermode="x unified")
    return mps_fig


# party attendance in time
def party_attendance(chosen_parties):
    if not chosen_parties:
        return {}
    # prepare data
    attendance_in_time_data = df[df.Party.isin(chosen_parties)].drop(columns=["Vote Title", "Vote description"])
    attendance_in_time_data['MP'] = 1
    attendance_in_time_data['Vote'] = attendance_in_time_data.Vote.apply(lambda x: 0 if x == "O" or x == "M" else 1)

    # make the computation
    attendance_in_time = attendance_in_time_data.groupby(["Vote Id", "Party"]).sum()
    attendance_in_time['result'] = attendance_in_time.Vote / attendance_in_time.MP * 100
    attendance_in_time = attendance_in_time.drop(columns=["Vote", "MP"]).unstack('Party').reset_index().set_index(
        'Vote Id')
    attendance_in_time.columns = attendance_in_time.columns.droplevel(0)

    # visualisation
    parties = attendance_in_time.rolling(500).mean()

    parties_fig = go.Figure()
    for col in parties.columns:
         value = parties[col]
         parties_fig.add_trace(go.Scatter(x=parties.index, y=value,
                                          name=col,
                                          mode='markers+lines',
                                          line=dict(shape='linear'),
                                          connectgaps=True
                                          )
                               )
    parties_fig.update_xaxes(title_text='Jednotlivá hlasování')
    parties_fig.update_yaxes(title_text='Procentuální účast poslanců jednotlivých stran')
    parties_fig.update_layout(hovermode="x unified")
    return parties_fig


# 10 worst mps by attendance per choosen parties
def worst_attendance_mps(chosen_parties):
    if not chosen_parties:
        return {}
    # prepare data
    attendance_mps_data = pd.concat([df_0, df_m])[["MP", "Vote", "Party"]]
    attendance_mps_data = attendance_mps_data[attendance_mps_data.Party.isin(chosen_parties)]
    attendance_mps_data = attendance_mps_data.drop(columns=["Party"])

    # get the computed data
    attendance_mps = attendance_mps_data.groupby("MP").count().sort_values("Vote", ascending=False).head(10)

    # make the visualisation
    figure = attendance_mps.plot(kind='bar', title="10. nejhorších poslanců podle docházky")

    figure.update_layout(showlegend=False)
    figure.update_xaxes(title_text='Poslanci')
    figure.update_yaxes(title_text='Počet hlasování kterých se poslanec neúčastnil')
    return figure


#prepare data for herd vote (shared)
def prepare_herd_vote():
    # prepare data
    herd_vote_data = pd.concat([df_a, df_n])[["MP", "Vote", "Party", "Vote Id"]]
    herd_vote_data["Vote_A"] = herd_vote_data.Vote.apply(lambda x: 1 if x == "A" else 0)
    herd_vote_data["Vote_N"] = herd_vote_data.Vote.apply(lambda x: 1 if x == "N" else 0)

    # get get sum of votes per party per vote
    herd_vote = herd_vote_data.groupby(["Vote Id", "Party"]).sum().reset_index()

    return herd_vote


#prepare data for herd mentality for each mps
def prepare_herd_data():
    herd_vote = prepare_herd_vote()
    # create helper column - is column Vote_A a majority ? 
    herd_vote['result'] = herd_vote["Vote_A"] > herd_vote["Vote_N"]

    # divide data into two parts - A is majority and N is majority
    herd_vote_n = herd_vote[herd_vote['result'] == False]
    herd_vote_a = herd_vote[herd_vote['result'] == True]

    # get all votes where parties had at least one vote opposite 
    herd_vote_n_res = herd_vote_n[herd_vote_n["Vote_A"] > 0].drop(columns=['Vote_A', 'Vote_N', 'result'])
    herd_vote_a_res = herd_vote_a[herd_vote_a["Vote_N"] > 0].drop(columns=['Vote_A', 'Vote_N', 'result'])

    # add another helper column to mark the opposite of majority vote
    herd_vote_n_res['Vote'] = "A"
    herd_vote_a_res['Vote'] = "N"

    # merge them together
    herd_vote_res = pd.concat([herd_vote_n_res, herd_vote_a_res])

    return herd_vote_res


def rebel_mps_prepare_data():
    herd_vote_res = prepare_herd_data()
    # stackoverflow magic :D 
    # it takes data and transform them into tuples and then filter with them original values
    # goal is to get mps that voted incorrectly (= against the party majority vote) 
    tuples = [tuple(x) for x in herd_vote_res.to_numpy()]
    result = df[pd.Series(list(zip(df['Vote Id'], df['Party'], df['Vote']))).isin(tuples)]

    return result


def rebel_mps_prepare_table(chosen_mp):
    rebel_mps_data = rebel_mps_prepare_data()
    chosen_mps = [chosen_mp]
    return rebel_mps_data[rebel_mps_data.MP.isin(chosen_mps)].to_dict('records')


def rebel_mps(chosen_parties):
    if not chosen_parties:
        return {}
    rebel_mps_data = rebel_mps_prepare_data()

    # get count of rebel actions for every MP and then choose the 10 worst 
    top_rebel_mps = rebel_mps_data[rebel_mps_data.Party.isin(chosen_parties)].MP.value_counts().head(10)
    figure = top_rebel_mps.plot(kind='bar',
                                title="10. největších rebelů (rebel=poslanec co hlasuje jinak než většina jeho strany)")

    figure.update_layout(showlegend=False)
    figure.update_xaxes(title_text='Poslanci')
    figure.update_yaxes(title_text='Počet hlasování ve kterých poslanec hlasoval jinak než jeho strana')

    return figure


def unity_party():
    herd_vote = prepare_herd_vote()
    # create helper column - is column Vote_A a majority ?
    herd_vote['result'] = herd_vote["Vote_A"] > herd_vote["Vote_N"]

    # divide data into two parts - A is majority and N is majority
    herd_vote_n = herd_vote[herd_vote['result'] == False]
    herd_vote_a = herd_vote[herd_vote['result'] == True]

    # get data where parties voted as one
    herd_vote_n_res_party = herd_vote_n[herd_vote_n["Vote_A"] != 0].drop(columns=['Vote_A', 'Vote_N', 'result'])
    herd_vote_a_res_party = herd_vote_a[herd_vote_a["Vote_N"] != 0].drop(columns=['Vote_A', 'Vote_N', 'result'])

    # add another helper column to mark the opposite of majority vote
    herd_vote_n_res_party['Vote'] = "A"
    herd_vote_a_res_party['Vote'] = "N"

    # merge them together
    herd_vote_res = pd.concat([herd_vote_n_res_party, herd_vote_a_res_party]).drop(columns=['Vote Id'])

    # group the count per party and plot the result
    figure = herd_vote_res.groupby("Party").count().sort_values("Vote", ascending=False).plot(
        kind='bar', title='Počet hlasování kdy strany hlasovala nejednotně (alespoň jeden poslanec hlasoval opačně)')

    figure.update_layout(showlegend=False)
    figure.update_xaxes(title_text='Strany')
    figure.update_yaxes(title_text='Počet hlasování ve kterých strana hlasovala nejednotně')

    return figure


def correlation_n(chosen_parties):
    if not chosen_parties or len(chosen_parties) < 3:
        return {}
    # prepare data
    corr_df_data_n = df_n[df_n.Party.isin(chosen_parties)].drop(columns=['Vote description', 'Vote Title', 'MP'])

    # compute the results
    corr_matrix_n = corr_df_data_n.groupby(["Vote Id", "Party"]).count().unstack('Party').fillna(0).corr()

    fig_n = px.imshow(corr_matrix_n, labels=dict(x="", y="", color="Míra podobnosti při hlasování NE"),
                      x=chosen_parties,
                      y=chosen_parties,
                      color_continuous_scale='Bluered_r')

    return fig_n


def correlation_a(chosen_parties):
    if not chosen_parties or len(chosen_parties) < 3:
        return {}
    # prepare data
    corr_df_data_a = df_a[df_a.Party.isin(chosen_parties)].drop(columns=['Vote description', 'Vote Title', 'MP'])

    # compute the results
    corr_matrix_a = corr_df_data_a.groupby(["Vote Id", "Party"]).count().unstack('Party').fillna(0).corr()

    fig_a = px.imshow(corr_matrix_a, labels=dict(x="", y="", color="Míra podobnosti při hlasování ANO"),
                      x=chosen_parties,
                      y=chosen_parties,
                      color_continuous_scale='Bluered_r')

    return fig_a


def correlation_mp_n(chosen_mps):
    if not chosen_mps or len(chosen_mps) < 3:
        return {}
    # prepare data
    corr_df_data_n = df_n[df_n.MP.isin(chosen_mps)].drop(columns=['Vote description', 'Vote Title', 'Party'])

    # compute the results
    corr_matrix_n = corr_df_data_n.groupby(["Vote Id", "MP"]).count().unstack('MP').fillna(0).corr()

    fig_n = px.imshow(corr_matrix_n, labels=dict(x="", y="", color="Míra podobnosti při hlasování NE"),
                      x=chosen_mps,
                      y=chosen_mps,
                      color_continuous_scale='Bluered_r')

    return fig_n


def correlation_mp_a(chosen_mps):
    if not chosen_mps or len(chosen_mps) < 3:
        return {}
    # prepare data
    corr_df_data_a = df_a[df_a.MP.isin(chosen_mps)].drop(columns=['Vote description', 'Vote Title', 'Party'])

    # compute the results
    corr_matrix_a = corr_df_data_a.groupby(["Vote Id", "MP"]).count().unstack('MP').fillna(0).corr()

    fig_a = px.imshow(corr_matrix_a, labels=dict(x="", y="", color="Míra podobnosti při hlasování ANO"),
                      x=chosen_mps,
                      y=chosen_mps,
                      color_continuous_scale='Bluered_r')

    return fig_a


# Show time-based number of MPs per Party
time_based_number_mps_data = df[df.Party != "ANO"].drop(columns=["Vote Title", "Vote", "Vote description"])

# computation
time_based_number_mps = time_based_number_mps_data.groupby(["Vote Id", "Party"]).count().unstack('Party').reset_index().set_index('Vote Id')
time_based_number_mps.columns = time_based_number_mps.columns.droplevel(0)

nr_mps_party_fig = time_based_number_mps.plot()
nr_mps_party_fig.update_xaxes(title_text='Jednotlivá hlasování')
nr_mps_party_fig.update_yaxes(title_text='Počet poslanců za danou stranu')

navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Docházka", href="#dochazka")),
        dbc.NavItem(dbc.NavLink("Hlasování", href="#hlasovani")),
        dbc.NavItem(dbc.NavLink("Hlasovácí koalice", href="#hlasovaci-koalice")),
        dbc.NavItem(dbc.NavLink("Ostatní", href="#ostatni")),
    ],
    brand="Vizualizace dat z hlasování v poslanecké sněmovně",
    brand_href="#output",
    color="primary",
    dark=True,
    fixed="top"
)

app.layout = html.Div([
    navbar,
    dbc.Container(children=[
        html.Div(id='output'),
        html.Br(),
        html.H2(children='Docházka', id="dochazka"),
        html.Br(),
        html.H4(children='Procentuální účast na hlasování'),
        dcc.Graph(
            id='party-attendance-percentual',
            figure=attendance_fig
        ),
        html.H4(children='Vývoj docházky jednotlivých poslanců na hlasování'),
        dcc.Dropdown(
            id='mps-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choices(df.MP.unique(), k=2),
            multi=True
        ),
        dcc.Graph(
            id='mps-attendance-graph',
            figure=mps_attendance([])
        ),
        html.H4(children='Vývoj docházky jednotlivých stran'),
        dcc.Dropdown(
            id='party-dropdown',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='party-attendance-in-time-graph',
            figure=party_attendance([])
        ),
        html.H3(children='10. nejhorších poslanců jednotlivých stran podle docházky'),
        dcc.Dropdown(
            id='party-dropdown-worst',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='mps-attendance-worst-graph',
            figure=worst_attendance_mps([])
        ),
        html.Br(),
        html.H2(children='Hlasování', id="hlasovani"),
        html.Br(),
        html.H4(children='Počet hlasování ve kterých strany hlasovali nejednotně'),
        dcc.Graph(
            id='unity',
            figure=unity_party()
        ),
        html.H4(children='10. největších rebelů jednotlivých stran'),
        dcc.Dropdown(
            id='party-dropdown-biggest',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='biggest-rebels-graph',
            figure=rebel_mps([])
        ),
        html.H4(children='Hlasování ve kterých daní poslanci hlasovali proti své straně'),
        dcc.Dropdown(
            id='mps-rebel-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choice(df.MP.unique())
        ),
        html.Br(),
        dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in ["Vote", "Vote Title", "Vote description"]],
            data=rebel_mps_prepare_table([]),
        ),
        html.Br(),
        html.H2(children='Hlasovácí koalice', id="hlasovaci-koalice"),
        html.Br(),
        html.H4(children='Korelace ve hlasování jednotlivých stran'),
        html.Span(children='Zvolte alespoň 3 strany'),
        dcc.Dropdown(
            id='correlation-party-dropdown',
            options=[{"label": item, "value": item} for item in df.Party.unique()],
            value=df.Party.unique(),
            multi=True
        ),
        dcc.Graph(
            id='correlation-n-graph',
            figure=correlation_n([])
        ),
        dcc.Graph(
            id='correlation-a-graph',
            figure=correlation_a([])
        ),
        html.H4(children='Korelace ve hlasování jednotlivých poslanců'),
        html.Span(children='Zvolte alespoň 3 poslance'),
        dcc.Dropdown(
            id='correlation-mps-dropdown',
            options=[{"label": item, "value": item} for item in df.MP.unique()],
            value=random.choices(df.MP.unique(), k=3),
            multi=True
        ),
        dcc.Graph(
            id='correlation-mps-n-graph',
            figure=correlation_mp_n([])
        ),
        dcc.Graph(
            id='correlation-mps-a-graph',
            figure=correlation_mp_a([])
        ),
        html.Br(),
        html.H2(children='Ostatní', id="ostatni"),
        html.Br(),
        html.H4(children='Vývoj počtu poslanců v jednotlivých stranách'),
        dcc.Graph(
            id='count-mps-a-graph',
            figure=nr_mps_party_fig
         ),
    ])
])


@app.callback(
    dash.dependencies.Output('mps-attendance-graph', 'figure'),
    [dash.dependencies.Input('mps-dropdown', 'value')])
def update_output(value):
    return mps_attendance(value)


@app.callback(
    dash.dependencies.Output('party-attendance-in-time-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown', 'value')])
def update_output(value):
    return party_attendance(value)


@app.callback(
    dash.dependencies.Output('mps-attendance-worst-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown-worst', 'value')])
def update_output(value):
    return worst_attendance_mps(value)


@app.callback(
    dash.dependencies.Output('biggest-rebels-graph', 'figure'),
    [dash.dependencies.Input('party-dropdown-biggest', 'value')])
def update_output(value):
    return rebel_mps(value)


@app.callback(
    dash.dependencies.Output('table', 'data'),
    [dash.dependencies.Input('mps-rebel-dropdown', 'value')])
def update_output(value):
    return rebel_mps_prepare_table(value)


@app.callback(
    dash.dependencies.Output('correlation-n-graph', 'figure'),
    [dash.dependencies.Input('correlation-party-dropdown', 'value')])
def update_output(value):
    return correlation_n(value)


@app.callback(
    dash.dependencies.Output('correlation-a-graph', 'figure'),
    [dash.dependencies.Input('correlation-party-dropdown', 'value')])
def update_output(value):
    return correlation_a(value)


@app.callback(
    dash.dependencies.Output('correlation-mps-a-graph', 'figure'),
    [dash.dependencies.Input('correlation-mps-dropdown', 'value')])
def update_output(value):
    return correlation_mp_a(value)


@app.callback(
    dash.dependencies.Output('correlation-mps-n-graph', 'figure'),
    [dash.dependencies.Input('correlation-mps-dropdown', 'value')])
def update_output(value):
    return correlation_mp_n(value)


if __name__ == '__main__':
    app.run_server(debug=True)
